/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.carsharing.system;

import java.io.IOException;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;

/** Supported class for HTTP call. */
public class HeaderSettingRequestCallback implements RequestCallback {
	final Map<String, String> requestHeaders;

	/** body field. */
	private String body;

	/**
	 * Method is to set body.
	 * 
	 * @param postBody
	 *            input body.
	 */
	public void setBody(final String postBody) {
		this.body = postBody;
	}

	/**
	 * Method is to set headers in request.
	 * 
	 * @param headers
	 */
	public HeaderSettingRequestCallback(final Map<String, String> headers) {
		this.requestHeaders = headers;
	}

	@Override
	public void doWithRequest(ClientHttpRequest request) throws IOException {
		final HttpHeaders clientHeaders = request.getHeaders();
		for (final Map.Entry<String, String> entry : requestHeaders.entrySet()) {
			clientHeaders.add(entry.getKey(), entry.getValue());
		}
		if (null != body) {
			request.getBody().write(body.getBytes());
		}
	}
}