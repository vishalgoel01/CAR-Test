/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.carsharing.system;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.springframework.http.client.ClientHttpResponse;

/** Supported class for Test cases. */
public class ResponseResults {

	/** response field of response result. */
	private final ClientHttpResponse theResponse;

	/** body field of response result. */
	private final String body;

	/** output Id field of response result. */
	static String outputId;

	/**
	 * Method is used to set the response.
	 * 
	 * @param response
	 *            input response.
	 * @throws IOException
	 */
	ResponseResults(final ClientHttpResponse response) throws IOException {
		this.theResponse = response;
		final InputStream bodyInputStream = response.getBody();
		final StringWriter stringWriter = new StringWriter();

		IOUtils.copy(bodyInputStream, stringWriter);
		this.body = stringWriter.toString();
		this.outputId = this.body;
	}

	/**
	 * Method is to get the response.
	 * 
	 * @return ClientHttpResponse response.
	 */
	ClientHttpResponse getTheResponse() {
		return theResponse;
	}

	/**
	 * Method is to get the response body.
	 * 
	 * @return response body.
	 */
	String getBody() {
		return body;
	}
}