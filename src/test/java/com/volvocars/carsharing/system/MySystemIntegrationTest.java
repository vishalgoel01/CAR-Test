/* Copyright (C) 2018 Volvo. All Rights Reserved. */
package com.volvocars.carsharing.system;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.junit.ClassRule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.connection.Container;
import com.palantir.docker.compose.connection.DockerPort;
import com.palantir.docker.compose.connection.State;

/** MySystemIntegrationTest test class. */
public class MySystemIntegrationTest {

	/** Class logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(MySystemIntegrationTest.class);

	@ClassRule
	public static DockerComposeRule docker = DockerComposeRule.builder().file("src/test/resources/docker-compose.yml")
			.build();

	@Test
	public void test_docker_compose_rule_up_container_users() throws IOException, InterruptedException {
		Container container = docker.containers().container("firstproject");
		container.up();
		assertTrue(container.state() == State.HEALTHY);
	}


	@Test
	public void getUsersTest() throws Exception {
		LOGGER.info("Executing getUsersTest test");
		@SuppressWarnings("deprecation")
		HttpClient client = new DefaultHttpClient();

		DockerPort container = docker.containers().container("firstproject").port(8080);
		String url = "http://" + container.getIp() + ":" + container.getExternalPort() + "/users";
		LOGGER.info("getUsersTest URL " + url);
		// HttpGet request = new HttpGet(url);
		// HttpResponse response = client.execute(request);
		// int statusCode = response.getStatusLine().getStatusCode();
		// LOGGER.info("getUsersTest: status code = " + statusCode);
		// assertTrue(statusCode == 401); // this need to be changed.
		// LOGGER.info("ending getUsersTest....");

		// Given
		HttpUriRequest request = new HttpGet(url);
		LOGGER.info("request " + request);
		// When
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		LOGGER.info("httpResponse " + httpResponse);
		LOGGER.info("(httpResponse.getStatusLine().getStatusCode() " + httpResponse.getStatusLine().getStatusCode());
		// Then
		assertTrue(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND);

	}

//	@Test
//	public void addUserGroupTest() throws Exception {
//		LOGGER.info("Executing addUserGroupTest test");
//		CloseableHttpClient client = HttpClients.createDefault();
//
//		DockerPort container = docker.containers().container("carsharing-users").port(8080);
//		String url = "http://" + container.getIp() + ":" + container.getExternalPort() + "/users";
//		LOGGER.info("addUserGroupTest URL " + url);
//		HttpPost httpPost = new HttpPost(url);
//
//		String json = "{\"groupId\": \"1111\",\"name\": \"HR\",\"accessLevel\": [{\"level\": \"OPEN_DOORS\"}]}";
//		StringEntity entity = new StringEntity(json);
//		httpPost.setEntity(entity);
//		httpPost.setHeader("Accept", "application/json");
//		httpPost.setHeader("Content-type", "application/json");
//
//		CloseableHttpResponse response = client.execute(httpPost);
//		LOGGER.info("addUserGroupTest Response code {}", response.getStatusLine().getStatusCode());
//		assertTrue(response.getStatusLine().getStatusCode() == 401);
//
//	}
//
//	@Test
//	public void geBookingTest() throws Exception {
//		LOGGER.info("executing geBookingTest....");
//		@SuppressWarnings("deprecation")
//		HttpClient client = new DefaultHttpClient();
//
//		DockerPort container = docker.containers().container("carsharing-bookings").port(8080);
//		String url = "http://" + container.getIp() + ":" + container.getExternalPort() + "/bookings/";
//		LOGGER.info("geBookingTest URL " + url);
//		HttpGet request = new HttpGet(url);
//		HttpResponse response = client.execute(request);
//		int statusCode = response.getStatusLine().getStatusCode();
//		LOGGER.info("geBookingTest status code " + statusCode);
//		assertTrue(statusCode == 401); // this need to be changed.
//		LOGGER.info("ending geBookingTest....");
//	}
//
//	@Test
//	public void postBookingTest() throws Exception {
//		LOGGER.info("Executing postBookingTest test");
//		CloseableHttpClient client = HttpClients.createDefault();
//
//		DockerPort container = docker.containers().container("carsharing-bookings").port(8080);
//		String url = "http://" + container.getIp() + ":" + container.getExternalPort() + "/bookings/";
//		LOGGER.info("postBookingTest URL " + url);
//		HttpPost httpPost = new HttpPost(url);
//
//		String json = "{\"wantedAccess\": [{\"access\": \"OPEN_DOORS\"}],\"vehicle\": {\"vin\": \"vin123\",\"admin\": {\"userId\": \"1111\","
//				+ "\"firstName\": \"Anders\",\"lastName\": \"Singh\"},\"model\": \"Toyota\",\"colour\": \"Black\"}, \"guest\": {\"userId\": \"22222\","
//				+ "\"firstName\": \"Deepak\",\"lastName\": \"Gakhar\"}, \"period\": { \"start\": \"2018-03-13T18:47:30.887Z\", \"end\": \"2018-03-13T18:47:30.887Z\"}}";
//		StringEntity entity = new StringEntity(json);
//		httpPost.setEntity(entity);
//		httpPost.setHeader("Accept", "application/json");
//		httpPost.setHeader("Content-type", "application/json");
//
//		CloseableHttpResponse response = client.execute(httpPost);
//		LOGGER.info("postBookingTest response code {}", response.getStatusLine().getStatusCode());
//		assertTrue(response.getStatusLine().getStatusCode() == 401);
//	}

}
